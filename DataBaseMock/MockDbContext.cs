﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DataBaseMock
{
    public class MockDbContext : MarshalByRefObject, IDbContext
    {
        public Person Get(int id)
        {
            Console.WriteLine($"Database was asked for {id}th person");
            return new Person()
            {
                FirstName = "first_name_" + id,
                LastName = "last_name_" + id,
                Id = id
            };
        }
    }
}
