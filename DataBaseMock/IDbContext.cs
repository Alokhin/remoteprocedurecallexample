﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CookComputing.XmlRpc;

namespace DataBaseMock
{
    public interface IDbContext
    {
        [XmlRpcMethod("person.get")]
        Person Get(int id);
    }
}
