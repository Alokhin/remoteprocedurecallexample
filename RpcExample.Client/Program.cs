﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataBaseMock;
using RpcExample.Shared;

namespace RpcExample.Client
{
    class Program
    {
        public static string ip = "127.0.0.1";

        static void Main(string[] args)
        {
            var rpcClient = RpcClient.CreateHttpRpcClient();
            var dbContext = rpcClient.GetService<IDbContext>($"http://{ip}:{Constants.HttpChannelPort}/{Constants.DataBaseServiceHost}");

            var person = dbContext.Get(3);
            Console.WriteLine(person.ToString());

            person = dbContext.Get(30);
            Console.WriteLine(person.ToString());

            person = dbContext.Get(5);
            Console.WriteLine(person.ToString());

            Console.ReadKey();
        }
    }
}
