﻿using System;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using CookComputing.XmlRpc;

namespace RpcExample.Client
{
    public class RpcClient
    {
        public RpcClient(IChannel channel)
        {
            ChannelServices.RegisterChannel(channel, false);
        }

        public T GetService<T>(string url)
        {
            return (T) Activator.GetObject(typeof(T), url);
        }

        public static RpcClient CreateHttpRpcClient()
        {
            HttpChannel channel = new HttpChannel(null, new XmlRpcClientFormatterSinkProvider(), null);
            return new RpcClient(channel);
        }
    }
}
