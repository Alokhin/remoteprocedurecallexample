﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using DataBaseMock;
using RpcExample.Shared;

namespace RpcExample.Server
{
    class Program
    {
        static void Main(string[] args)
        {
            var server = RpcServer.CreateHttpRpcServer("MyHttpChannel", Constants.HttpChannelPort);
            server.RegisterService<MockDbContext>(Constants.DataBaseServiceHost, WellKnownObjectMode.Singleton);
            Console.WriteLine("Press <ENTER> to shutdown");
            Console.ReadLine();
        }
    }
}
