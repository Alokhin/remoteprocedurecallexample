﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Text;
using System.Threading.Tasks;
using CookComputing.XmlRpc;

namespace RpcExample.Server
{
    public class RpcServer
    {
        public RpcServer(IChannel channel)
        {
            ChannelServices.RegisterChannel(channel, false);
        }

        public void RegisterService<T>(string objectUrl, WellKnownObjectMode mode)
        {
            RemotingConfiguration.RegisterWellKnownServiceType(typeof(T), objectUrl, mode);
        }

        public static RpcServer CreateHttpRpcServer(IDictionary props)
        {
            HttpChannel channel = new HttpChannel(props, null, new XmlRpcServerFormatterSinkProvider());
            return new RpcServer(channel);
        }

        public static RpcServer CreateHttpRpcServer(string name, int port)
        {
            IDictionary props = new Hashtable();
            props["name"] = name;
            props["port"] = port;
            return RpcServer.CreateHttpRpcServer(props);
        }
    }
}
