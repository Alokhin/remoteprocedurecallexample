﻿namespace RpcExample.Shared
{
    public static class Constants
    {
        private static string _serviceSuffix = ".rem";

        public static string DataBaseServiceHost => "database" + _serviceSuffix;
        public static int HttpChannelPort => 5678;
    }
}
